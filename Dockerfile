FROM python:3.10-slim

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

ARG APP_DIR=/app
ARG FILES_DIR=/files

RUN mkdir ${APP_DIR} && \
    mkdir ${FILES_DIR}

WORKDIR ${APP_DIR}

ADD . ${APP_DIR}


RUN pip install --upgrade pip
RUN echo ls ${APP_DIR}
RUN pip install -r ${APP_DIR}/book/requirements.txt


