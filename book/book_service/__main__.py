import os

from aiohttp import web

from book_service.api.v0 import routes as v0_routes
from book_service.db.base import Base, engine
from book_service.middleware import error_middleware

app = web.Application()
app.router.add_routes(v0_routes)
app.middlewares.append(error_middleware)


def run_app():
    Base.metadata.create_all(engine)
    web.run_app(app, port=8000)


if __name__ == "__main__":
    run_app()
