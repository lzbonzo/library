import uuid
from enum import Enum as EnumType

from book_service.db.base import Base
from sqlalchemy import (UUID, Boolean, Column, DateTime, Enum, ForeignKey,
                        String, Table, text)
from sqlalchemy.orm import Mapped, relationship


class Genre(EnumType):
    FICTION = "Fiction"
    HORROR = "Horror"
    NON_FICTION = "Non-Fiction"
    POETRY = "Poetry"
    SCIENCE_FICTION = "Science Fiction"


book_author_association = Table(
    "book_author",
    Base.metadata,
    Column("book_id", UUID, ForeignKey("books.id"), primary_key=True),
    Column("author_id", UUID, ForeignKey("authors.id"), primary_key=True),
)


class Author(Base):
    __tablename__ = "authors"

    required_fields = ["name", "last_name"]

    id: Mapped[uuid.UUID] = Column(
        UUID(as_uuid=False), primary_key=True, server_default=text("gen_random_uuid()")
    )
    name: Mapped[str] = Column(String, nullable=False)
    second_name: Mapped[str] = Column(String, nullable=True)
    last_name: Mapped[str] = Column(String, nullable=False)
    birthday: Mapped[DateTime] = Column(DateTime, nullable=True)
    death: Mapped[DateTime] = Column(DateTime, nullable=True)
    books: Mapped[list["Book"]] = relationship(
        secondary=book_author_association, back_populates="authors"
    )

    def dict(self) -> dict:
        data = super().dict()
        data["books"] = [author.id for author in self.books]
        return data


class Book(Base):
    __tablename__ = "books"

    required_fields = ["title", "authors", "genre", "date_published"]

    id: Mapped[uuid.UUID] = Column(
        UUID(as_uuid=False), primary_key=True, server_default=text("gen_random_uuid()")
    )
    title: Mapped[str] = Column(String, nullable=False)
    date_published: Mapped[DateTime] = Column(DateTime, nullable=False)
    genre: Mapped[str] = Column(Enum(Genre), nullable=False)
    is_denied: Mapped[bool] = Column(Boolean, default=False)
    authors: Mapped[list["Author"]] = relationship(
        secondary=book_author_association, back_populates="books"
    )
    file: Mapped[str] = Column(String, nullable=False)

    def dict(self) -> dict:
        data = super().dict()
        data["authors"] = [author.id for author in self.authors]
        return data
