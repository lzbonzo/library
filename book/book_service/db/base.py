import os

from sqlalchemy import create_engine

from sqlalchemy.orm import DeclarativeBase, sessionmaker

from book_service.constants import DEFAULT_POSTGRES_USER, DEFAULT_POSTGRES_PASSWORD

db_user = os.getenv("POSTGRES_USER", DEFAULT_POSTGRES_USER)
db_password = os.getenv("POSTGRES_PASSWORD", DEFAULT_POSTGRES_PASSWORD)
db_name = os.getenv("POSTGRES_DB")
db_host = os.getenv("POSTGRES_HOST")


class Base(DeclarativeBase):
    required_fields = None

    def dict(self) -> dict:
        return {field.name:getattr(self, field.name) for field in self.__table__.c}

    @classmethod
    def validate(cls, **kwargs):
        for field in cls.required_fields:
            value = kwargs[field]
            if not value:
                raise ValueError(f"Field {field} is required")
            if isinstance(value, str):
                kwargs[field] = kwargs[field].capitalize()


engine = create_engine(f"postgresql+psycopg2://{db_user}:{db_password}@{db_host}/{db_name}")
Session = sessionmaker(bind=engine)
