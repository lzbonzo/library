import os
import uuid

import orjson
from aiohttp import web
from aiohttp.web_request import Request
from aiohttp.web_response import Response, StreamResponse
from book_service.db.base import Session
from book_service.db.models import Author, Book
from sqlalchemy import text
from sqlalchemy.exc import DataError

routes = web.RouteTableDef()
session = Session()


@routes.post("/authors")
async def create_author(request: Request) -> Response:
    data = await request.json()
    Author.validate(**data)
    new_author = Author(**data)
    session.add(new_author)
    session.commit()
    return web.json_response(new_author.dict())


@routes.get("/authors")
async def get_authors(request: Request) -> Response:
    query_params = request.rel_url.query
    result = session.query(Author).filter_by(**query_params)
    return Response(body=orjson.dumps([item.dict() for item in result]))


@routes.post("/books")
async def create_book(request: Request) -> Response:
    data = await request.post()
    data = dict(data)
    Book.validate(**data)

    authors_ids = orjson.loads(data.pop("authors"))
    authors = session.query(Author).filter(Author.id.in_(authors_ids)).all()
    if not authors or not len(authors_ids) == len(authors):
        raise ValueError("Can't find all authors")

    file_data = data["file"].file.read()
    file_path = os.path.join("/files", str(uuid.uuid4()))
    with open(file_path, "wb") as f:
        f.write(file_data)
        data["file"] = file_path

    new_book = Book(**data)
    session.add(new_book)
    new_book.authors = authors
    try:
        session.commit()
    except DataError as e:
        os.remove(file_path)
        raise e

    return Response(body=orjson.dumps(new_book.dict()))


@routes.get("/books")
async def get_books(request: Request) -> Response:
    query_params = request.rel_url.query
    authors_ids = query_params.getall("authors", None)
    query_params = dict(query_params)
    query_params.pop("authors", None)
    raw_query = (
        f"select {', '.join(Book.__table__.columns.keys())}, array_agg(ba.author_id) authors from books b\n"
        f"join book_author ba on b.id = ba.book_id\n"
    )
    if authors_ids:
        ids_string = "', '".join(authors_ids)
        raw_query += f"where ba.author_id in ('{ids_string}')\n"
    if query_params:
        start = "where\n" if not authors_ids else "and\n"
        raw_query += start
        raw_query += f"\nand ".join(
            [f"{key}='{value}'" for key, value in query_params.items()]
        )
    raw_query += "group by b.id;"

    result = session.execute(text(raw_query)).all()
    return Response(body=orjson.dumps([item._asdict() for item in result]))


@routes.get("/files/{uuid}")
async def get_files(request: Request) -> StreamResponse:
    file_uuid = request.match_info["uuid"]
    file_path = os.path.join("/files", file_uuid)

    response = StreamResponse(
        status=200,
        reason="OK",
        headers={
            "Content-Type": "application/octet-stream",
            "CONTENT-DISPOSITION": f"attachment;filename={file_uuid}",
        },
    )

    await response.prepare(request)
    try:
        with open(file_path, "rb") as f:
            await response.write(f.read())
    except FileNotFoundError:
        raise web.HTTPNotFound(text=f"File {file_uuid} is not found")
    return response
