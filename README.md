Run `docker-compose up` from project root

## Authors

#### Request
To create new author send request to

`POST http://localhost:8000/authors`

`curl --request POST \
  --url http://localhost:8000/authors \
  --header 'Content-Type: application/json' \
  --data ' 
     {
        "last_name": "Remarque",
        "second_name": "Maria",
        "name": "Erich"
    }'
`

OR

```json
{
	"last_name": "Remarque",
	"second_name": "Maria",
	"name": "Erich"
}
```
#### Expected response
```json
{
	"id": "6c99a25e-4b5e-4412-aa0f-cd935a17283d",
	"name": "Erich",
	"second_name": "Maria",
	"last_name": "Remarque",
	"birthday": null,
	"death": null,
	"books": []
}
```

`GET http://localhost:8000/authors`

To filter authors use query params: "id", "name", "second_name", "last_name"

#### Request example

`GET http://localhost:8000/authors?name=Erich&last_name=Remarque`

`curl --request GET \
  --url 'http://localhost:8000/authors?name=Erich&last_name=Remarque'`

#### Expected response

```json
[
	{
		"id": "38b57563-40ab-409d-9ff6-efedb9b95b21",
		"name": "Erich",
		"second_name": "Maria",
		"last_name": "Remarque",
		"birthday": null,
		"death": null,
		"books": []
	}
]
```

## Books

To create new book send request to

`POST http://localhost:8000/books`

`
curl --request POST \
  --url http://localhost:8000/books \
  --header 'Content-Type: multipart/form-data' \
  --form 'title=Very Second Cool book' \
  --form 'authors=["f1136a4c-0cee-4a51-b0ca-c2821f816e87"]' \
  --form genre=FICTION \
  --form file=<path_to_file> \
  --form date_published=09-30-2018
`

OR

![](https://gitlab.com/lzbonzo/library/-/raw/main/img.png?ref_type=heads)

#### GENRES ARE LISTED IN ENUM
    FICTION
    HORROR
    NON_FICTION
    POETRY
    SCIENCE_FICTION

#### Expected response
```json

{
	"id": "1dabe19b-a032-41fe-b5cd-01b6fdc982fc",
	"title": "Night In Lisbon",
	"date_published": "2018-09-30T00:00:00",
	"genre": "Fiction",
	"is_denied": false,
	"file": "/files/4987b5a9-2b2a-4219-b035-f051766c3656",
	"authors": [
		"f1136a4c-0cee-4a51-b0ca-c2821f816e86"
	]
}
```

`GET http://localhost:8000/books`

To filter authors use query params: "id", "authors", "title", "date_published"

#### Request example

`GET http://localhost:8000/books?title=Night%20In%20Lisbon`
`GET http://localhost:8000/books?date_published=2018-09-30`
`GET http://localhost:8000/books?authors=f1136a4c-0cee-4a51-b0ca-c2821f816e86&authors=38b57563-40ab-409d-9ff6-efedb9b95b21`

`curl --request GET \
  --url 'http://localhost:8000/authors?name=Erich&last_name=Remarque'`

#### Expected response

```json
[
	{
		"id": "e56253d9-bb2c-4905-ad43-6ec325f11dd4",
		"title": "Very Second Cool book",
		"date_published": "2018-09-30T00:00:00",
		"genre": "NON_FICTION",
		"is_denied": false,
		"file": "/files/41be1102-192f-4d50-b3e4-58662b1736eb",
        "authors": [
          "f1136a4c-0cee-4a51-b0ca-c2821f816e86",
          "38b57563-40ab-409d-9ff6-efedb9b95b21"
        ]
	},
	{
		"id": "3a715dec-89f6-42aa-8016-d37255f922b5",
		"title": "Three Comrades",
		"date_published": "2018-09-30T00:00:00",
		"genre": "FICTION",
		"is_denied": false,
		"file": "/files/b3d238f1-7ca9-4ba4-9d61-60db67beea48",
        "authors": [
          "f1136a4c-0cee-4a51-b0ca-c2821f816e86"
        ]
	},
	{
		"id": "1dabe19b-a032-41fe-b5cd-01b6fdc982fc",
		"title": "Night In Lisbon",
		"date_published": "2018-09-30T00:00:00",
		"genre": "FICTION",
		"is_denied": false,
		"file": "/files/4987b5a9-2b2a-4219-b035-f051766c3656",
        "authors": [
          "f1136a4c-0cee-4a51-b0ca-c2821f816e86"
        ]
	}
]
```

## FILES

To download file send request:

`
GET http://localhost:8000/<file_path_from_book>`
`

#### Request example
`
curl --request GET \
  --url http://localhost:8000/files/94335317-4d67-4da9-a59e-7894542f16e9sd
`
